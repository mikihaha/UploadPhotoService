package com.mikihaha.app.uploadservicedemo.camera;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class CameraConstant {
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final int PREVIEW_WIDTH = 640;
    public static final int PREVIEW_HEIGHT = 480;

    public static final int NUMBER_SOUND = 5;
}
