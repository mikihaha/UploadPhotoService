package com.mikihaha.app.uploadservicedemo.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.mikihaha.app.uploadservicedemo.service.UploadService;

public class BootReceiver extends BroadcastReceiver {
    public static final String TAG = BootReceiver.class.getName();

    public static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(BOOT_COMPLETED)) {
            Log.e(TAG, "onReceive: BOOT");

            PollingUtils.startPollingService(context,
                    UploadService.class,
                    UploadService.ACTION);
        }
    }
}
