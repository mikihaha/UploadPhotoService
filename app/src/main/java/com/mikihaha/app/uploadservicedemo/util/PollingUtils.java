package com.mikihaha.app.uploadservicedemo.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class PollingUtils {
    public static final String TAG = PollingUtils.class.getName();
    public static final int REQUEST_CODE = 666;   //seconds
    public static final long POLLING_INTERVAL = 180000;   //3 seconds in millis

    public static void startPollingService(Context context, Class<?> cls, String action) {
        Log.e(TAG, "startPollingService: ");
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);

        Intent intent = new Intent(context, cls);
        intent.setAction(action);
        PendingIntent pendingIntent = PendingIntent.getService(context, REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        long triggerAtTime = SystemClock.elapsedRealtime();

        manager.setRepeating(AlarmManager.ELAPSED_REALTIME,
                triggerAtTime,
                POLLING_INTERVAL,
                pendingIntent);

    }

    public static void stopPollingService(Context context, Class<?> cls, String action) {
        AlarmManager manager = AlarmManagerProvider.getAlarmManager(context);
        Intent intent = new Intent(context, cls);
        intent.setAction(action);

        PendingIntent pendingIntent = PendingIntent.getService(context,
                REQUEST_CODE,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        manager.cancel(pendingIntent);
    }
}
