package com.mikihaha.app.uploadservicedemo.camera;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.AudioManager;
import android.media.FaceDetector;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mikihaha.app.uploadservicedemo.R;
import com.mikihaha.app.uploadservicedemo.camera.presenter.CameraPresenter;
import com.mikihaha.app.uploadservicedemo.camera.presenter.CameraPresenterImpl;
import com.mikihaha.app.uploadservicedemo.view.OneFaceView;
import com.mikihaha.app.uploadservicedemo.view.StorageDialogFragment;

import java.io.IOException;
import java.util.List;

public class CameraActivity extends AppCompatActivity implements
        View.OnClickListener,
        SurfaceHolder.Callback,
        Camera.PreviewCallback {
    private static final String TAG = "CameraActivity";
    private static final String FOLDER_NAME = "/MikihahaCamera";
    private static final int DEFAULT_CAMERA_ID = 0; //back camera at Mibo

    private volatile boolean mIsCameraOpen = false;
    private boolean mIsTakePicture = true;  //for check is taking picture or taking video
    private boolean mIsTake = false;
    private boolean mNeedDetectFace = false;
    private boolean mNeedCountDown = false;

    private Camera mCamera = null;
    private View mTakingAnim;
    private OneFaceView mFaceMaskView;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mHolder;
    private FaceDetector mFaceDetector;

    private SoundPool mSoundPool;
    private FrameLayout mFrameLayout;
    private ImageButton mFaceButton, mShotButton, mTimerButton, mCloseButton, mBackButton, mConfirmButton;
    private TextView mConfirmTextView;
    private View mCaptureView;
    private View mConfirmView;

    private byte[] mCurrentData;

    private StorageDialogFragment mStorageDialogFragment;
    private int mSoundExit;
    private int mSoundConfirm;
    private int mSoundStoreDone;
    private int mSoundShootSmile;
    private int mAlertId;
    private TextView mCountDownTextView;

    CameraPresenter mCameraPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCameraPresenter = new CameraPresenterImpl();

        mStorageDialogFragment = new StorageDialogFragment();

        mTimerButton = (ImageButton) findViewById(R.id.btn_timer);
        mShotButton = (ImageButton) findViewById(R.id.btn_shot);
        mFaceButton = (ImageButton) findViewById(R.id.btn_face);
        mCloseButton = (ImageButton) findViewById(R.id.btn_close);
        mBackButton = (ImageButton) findViewById(R.id.btn_back);
        mConfirmButton = (ImageButton) findViewById(R.id.btn_confirm);
        mConfirmTextView = (TextView) findViewById(R.id.tv_confirm);

        mTakingAnim = (View) findViewById(R.id.capturing);
        mFaceMaskView = (OneFaceView) findViewById(R.id.faceMask);
        mFaceMaskView.setVisibility(View.GONE);
        mFrameLayout = (FrameLayout) findViewById(R.id.camera_preview);

        mCaptureView = findViewById(R.id.rv_capture);
        mConfirmView = findViewById(R.id.rv_confirm);

        mCountDownTextView = (TextView) findViewById(R.id.tv_count_down);

//        mCameraPresenter.initFlurryTools(CameraActivity.this, mFlurryAgentListener);
        hideDecorView();
//        startObserve();
//        mCameraPresenter.getStorageInfo();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        openCamera();

        mFaceButton.setOnClickListener(this);
        mShotButton.setOnClickListener(this);
        mTimerButton.setOnClickListener(this);
        mCloseButton.setOnClickListener(this);
        mBackButton.setOnClickListener(this);
        mConfirmButton.setOnClickListener(this);

        mSurfaceView = new SurfaceView(this);

        mHolder = mSurfaceView.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        mFrameLayout.addView(mSurfaceView, 0);

//        mFaceDetector = new FaceDetector(this);
//        mFaceDetector.init(CameraConstant.PREVIEW_WIDTH, CameraConstant.PREVIEW_HEIGHT);

        // SOUND
        mSoundPool = new SoundPool(CameraConstant.NUMBER_SOUND, AudioManager.STREAM_MUSIC, 5);
        mAlertId = mSoundPool.load(this, R.raw.countdown_1s, 1);
        mSoundExit = mSoundPool.load(this, R.raw.btn_close_2, 1);
        mSoundConfirm = mSoundPool.load(this, R.raw.msg_01, 1);
        mSoundStoreDone = mSoundPool.load(this, R.raw.state_success_2, 1);
        mSoundShootSmile = mSoundPool.load(this, R.raw.btn_shoot_smile, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();

//        this.mFaceDetector.release();
        closeCamera();

        mSoundPool.stop(mSoundExit);
        mSoundPool.stop(mSoundConfirm);
        mSoundPool.stop(mSoundStoreDone);
        mSoundPool.stop(mSoundShootSmile);
        mSoundPool.release();

//        mGifView.stopPlay();
        finish();
//        FlurryAnalyticsHelper.endTimedEvent(EVENT_DURATION_TIME);
    }

    public synchronized void openCamera() {
        Log.d(TAG, "open camera ");
        if (mIsCameraOpen) {
            return;
        }
        if (mCamera == null) {
            mCamera = checkCamera(this);
        }
        if (mCamera != null) {
            Camera.Parameters para = mCamera.getParameters();
            para.setPreviewSize(CameraConstant.PREVIEW_WIDTH, CameraConstant.PREVIEW_HEIGHT);
            mCamera.setParameters(para);
            mIsCameraOpen = true;
        } else {
            Log.d(TAG, "camera open failed.");
        }
    }

    private void closeCamera() {

        Log.d(TAG, "closeCamera");

        if (this.mCamera != null) {
            this.mCamera.setOneShotPreviewCallback(null);

            try {
                this.mCamera.setPreviewDisplay(null);
            } catch (IOException var2) {
                var2.printStackTrace();
            }

            mSurfaceView.getHolder().removeCallback(this);
            this.mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            this.mCamera.release();
            this.mCamera = null;
            this.mIsCameraOpen = false;
        }
    }

    private Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            mFaceMaskView.setVisibility(View.GONE);
            showConfirmView();
            mCurrentData = data;
        }
    };

    private void playCountdownAnim() {
        Log.d(TAG, "playCountdownAnim()");
//        mGifView.setVisibility(View.VISIBLE);
        //TODO:COUNT-DOWN
    }


    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
//        List<Rect> faceRectList = this.mFaceDetector.findFaces(data);
//        this.mFaceMaskView.updateFaceRect(faceRectList);

        if (this.mIsCameraOpen) {
            this.mCamera.setOneShotPreviewCallback(this);
        }

        if (mIsTake) { //take photo

            Log.d(TAG, "mNeedCountDown = " + mNeedCountDown + ",mNeedDetectFace = " + mNeedDetectFace);
            if (!mNeedCountDown && !mNeedDetectFace) {  //only take photo
                Log.v(TAG, "only take photo");
                animationToTakePicture();
                mIsTake = false;

            } else if (mNeedCountDown && !mNeedDetectFace) { // take photo need countdown
                Log.v(TAG, "take photo need countdown ");
                playCountdownAnim();
                mIsTake = false;

//            } else if (!mNeedCountDown && faceRectList.size() > 0) { // take photo need detect face
//                Log.v(TAG, "take photo need detect face,faceRectList.size() = " + faceRectList.size());
//                animationToTakePicture();
//                mIsTake = false;
//
//            } else if (faceRectList.size() > 0) { // take photo need detect face and countdown
//                Log.v(TAG, " take photo need detect face and countdown,faceRectList.size() = " + faceRectList.size());
//                playCountdownAnim();
//                mIsTake = false;

            }
        }
    }

    public void animationToTakePicture() {
        Log.d(TAG, "onAutoFocus and animation");
        Animation am = new AlphaAnimation(1.0f, 0.0f);
        am.setDuration(800);
        // 將動畫參數設定到圖片並開始執行動畫
        mTakingAnim.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mTakingAnim.setVisibility(View.VISIBLE);
        mTakingAnim.startAnimation(am);

        mSoundPool.stop(mAlertId);
        mTakingAnim.setVisibility(View.INVISIBLE);

        if (mIsTakePicture) {
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    if (success) {
                        Log.d(TAG, "AutoFocus success!");
                        mCamera.takePicture(null, null, mPictureCallback);
                        mIsTake = false;

                    } else {
                        Log.d(TAG, "AutoFocus did not success!");
                        mCamera.takePicture(null, null, mPictureCallback);
                        mIsTake = false;
                    }
                }
            });

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "1.surfaceCreated");
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "2.surfaceChanged");
        if (mHolder.getSurface() == null) {
            return;
        }
        try {
            mCamera.setPreviewDisplay(holder);

        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }


        mCamera.startPreview();
        mCamera.setOneShotPreviewCallback(this);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "3.surfaceDestroyed");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_face:
                if (mFaceButton.isSelected()) {
                    mFaceButton.setSelected(false);
                    mNeedDetectFace = false;
                    mFaceMaskView.setVisibility(View.GONE);
                } else {
                    mSoundPool.play(mSoundShootSmile, 1.0F, 1.0F, 0, 0, 1.0F);

                    mFaceButton.setSelected(true);
                    mNeedDetectFace = true;
                    mFaceMaskView.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.btn_shot:
                mIsTake = true;
                break;
            case R.id.btn_timer:
                if (mTimerButton.isSelected()) {
                    mTimerButton.setSelected(false);
                    mNeedCountDown = false;
                    if(mCountDownTextView.getVisibility() == View.VISIBLE) {
                        mCountDownTextView.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mTimerButton.setSelected(true);
                    mNeedCountDown = true;
                }
                break;
            case R.id.btn_close:
                mSoundPool.play(mSoundExit, 1.0F, 1.0F, 0, 0, 1.0F);
                finish();
                break;
            case R.id.btn_back:
                mBackButton.setSelected(true);
                closeConfirmView();
                break;
            case R.id.btn_confirm:
                Log.d(TAG, "onClick: btn_confirm");
                Log.d(TAG, "onClick: " + mConfirmButton.isSelected());
                if (mConfirmButton.isSelected()) {
                    mSoundPool.play(mSoundStoreDone, 1.0F, 1.0F, 0, 0, 1.0F);
                    closeConfirmView();
                } else {
                    mCameraPresenter.saveCurrentData(mCurrentData, CameraActivity.this);
                    mConfirmButton.setSelected(true);
                    mConfirmTextView.setText(R.string.confirm_done);
                }
                Log.d(TAG, "onClick: " + mConfirmButton.isSelected());
                break;
        }
    }

    private Camera checkCamera(Context context) {
     /*Check device has a camera*/
        Camera c = null;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.d("camera", "This device has camera!");
            int cameraId = DEFAULT_CAMERA_ID;
            c = Camera.open(cameraId);
        } else {
            Log.d("camera", "This device has no camera!");
        }
        return c;
    }

    public void hideDecorView() {
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    private void closeConfirmView() {
        mCamera.startPreview();
        mConfirmView.setVisibility(View.GONE);
        mCaptureView.setVisibility(View.VISIBLE);
        Log.d(TAG, "closeConfirmView: " + mNeedDetectFace);

        if (mNeedDetectFace) {
            mFaceMaskView.setVisibility(View.VISIBLE);
        }
        else {
            mFaceMaskView.setVisibility(View.GONE);
        }
    }

    private void showConfirmView() {
        mSoundPool.play(mSoundConfirm, 1.0F, 1.0F, 0, 0, 1.0F);

        mCaptureView.setVisibility(View.INVISIBLE);
        mConfirmView.setVisibility(View.VISIBLE);
        mConfirmTextView.setText(R.string.confirm_check);
        mConfirmButton.setSelected(false);
        mBackButton.setSelected(false);
    }
}
