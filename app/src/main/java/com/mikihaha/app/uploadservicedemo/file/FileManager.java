package com.mikihaha.app.uploadservicedemo.file;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.mikihaha.app.uploadservicedemo.camera.CameraConstant;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class FileManager {
    private static final String TAG = "FileManager";
    private static final String FOLDER_NAME = "MikihahaCamera";

    private static final FileManager ourInstance = new FileManager();
    public static FileManager getInstance() {
        return ourInstance;
    }

    /** Create a file Uri for saving an image or video */
    private static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), FOLDER_NAME);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == CameraConstant.MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == CameraConstant.MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    public void deletePhoto(String imagePath) {
        File imageFile = new File(imagePath);
        boolean deleted = imageFile.delete();
        Log.d(TAG, "deletePhoto: " + deleted);
    }

    public File[] getDataListOfFolder() {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), FOLDER_NAME);
        File[] fileArray = file.listFiles();
        if(fileArray != null && fileArray.length != 0) {
            for (File singleFile : fileArray) {
                Log.d(TAG, "getDataListOfFolder: " + singleFile.getAbsolutePath());
            }
        }

        return fileArray;
    }
}
