package com.mikihaha.app.uploadservicedemo.camera.presenter;

import android.content.Context;

/**
 * Created by mikihaha on 2018/5/2.
 */

public interface CameraPresenter {
    void saveCurrentData(byte[] data, Context context);
//    void getStorageInfo();
//    void initFlurryTools(Context context, FlurryAgentListener listener);
}
