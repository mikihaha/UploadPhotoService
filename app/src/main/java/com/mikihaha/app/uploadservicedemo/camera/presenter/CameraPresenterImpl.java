package com.mikihaha.app.uploadservicedemo.camera.presenter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mikihaha.app.uploadservicedemo.camera.CameraConstant;
import com.mikihaha.app.uploadservicedemo.file.FileManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class CameraPresenterImpl implements CameraPresenter {
    public static final String TAG = CameraPresenterImpl.class.getName();

    @Override
    public void saveCurrentData(byte[] data, Context context) {
        File pictureFile = FileManager.getInstance().getOutputMediaFile(CameraConstant.MEDIA_TYPE_IMAGE);
        if (pictureFile == null) {
            Log.d(TAG, "Error creating media file, check storage permissions: ");
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            FileManager.getInstance().getDataListOfFolder();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Toast.makeText(context, "影像儲存錯誤!", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
    }
}
