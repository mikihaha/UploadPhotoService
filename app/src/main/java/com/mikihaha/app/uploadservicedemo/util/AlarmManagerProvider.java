package com.mikihaha.app.uploadservicedemo.util;

import android.app.AlarmManager;
import android.content.Context;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class AlarmManagerProvider {
    public static final String TAG = AlarmManagerProvider.class.getName();
    private static AlarmManager sAlarmManager;

    public static synchronized AlarmManager getAlarmManager(Context context) {
        if (sAlarmManager == null) {
            sAlarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        return sAlarmManager;
    }
}
