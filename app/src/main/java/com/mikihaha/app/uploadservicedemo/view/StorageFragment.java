package com.mikihaha.app.uploadservicedemo.view;

import android.app.Fragment;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.mikihaha.app.uploadservicedemo.R;

public class StorageFragment extends Fragment {

    private Button mOkButton;
    private StorageListener mStorageListener;

    public void setStorageListener(StorageListener storageListener) {
        this.mStorageListener = storageListener;
    }

    public interface StorageListener {
        void onOkButtonClick();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_storage, container, false);

        mOkButton = (Button) view.findViewById(R.id.btn_ok);
        mOkButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                onOkButtonClick();
            }
        });

        return view;
    }

    private void onOkButtonClick(){
        if(mStorageListener != null) {
            mStorageListener.onOkButtonClick();
        }
    }

}
