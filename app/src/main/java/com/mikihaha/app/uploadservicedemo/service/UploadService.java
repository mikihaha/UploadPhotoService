package com.mikihaha.app.uploadservicedemo.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.widget.Toast;

import com.mikihaha.app.uploadservicedemo.file.FileManager;

import java.io.File;

public class UploadService extends Service {
    public static final String TAG = UploadService.class.getName();
    public static final String ACTION = "com.mikihaha.app.uploadservicedemo.service";
    public static final String NAME_HANDLER_THREAD = "UploadHandlerThread";
    public static final int MAS_UPLOAD_PHOTO = 777;
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private HandlerThread mHandlerThread;

    private  final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.e(TAG, "handleMessage: ");
            switch (msg.what) {
                case MAS_UPLOAD_PHOTO:
                    Log.d(TAG, "handleMessage: MAS_UPLOAD_PHOTO");

                    File[] fileList = FileManager.getInstance().getDataListOfFolder();

                    for(File singleFile : fileList) {
                        //TODO:call your own upload function
                        Toast.makeText(getApplicationContext(), "Uploading...", Toast.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    }

    @Override
    public void onCreate() {
        //TODO:do initial work here
        Log.e(TAG, "onCreate: ");
        mHandlerThread = new HandlerThread(NAME_HANDLER_THREAD,
                Process.THREAD_PRIORITY_BACKGROUND);
        mHandlerThread.start();

        mServiceLooper = mHandlerThread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: ");

        File[] fileList = FileManager.getInstance().getDataListOfFolder();

        if(fileList != null && fileList.length != 0) {
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = startId;
            msg.what = MAS_UPLOAD_PHOTO;
            mServiceHandler.sendMessage(msg);
        }

        return START_STICKY;
    }

    public UploadService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy: ");
        mServiceHandler.removeCallbacksAndMessages(null);
        stopHandlerThread(mHandlerThread);
    }

    private void stopHandlerThread(HandlerThread handlerThread) {
        handlerThread.quit();
        handlerThread.interrupt();
    }
}
