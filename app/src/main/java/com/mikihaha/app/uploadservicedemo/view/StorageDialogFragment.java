package com.mikihaha.app.uploadservicedemo.view;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.mikihaha.app.uploadservicedemo.R;

/**
 * Created by mikihaha on 2018/5/2.
 */

public class StorageDialogFragment extends DialogFragment {
    public static final String TAG = StorageDialogFragment.class.getName();
    private DialogInterface.OnDismissListener dismissListener;
    private Button mOkButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_storage, container, false);

        mOkButton = (Button) view.findViewById(R.id.btn_ok);
        mOkButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        return view;
    }
}
